package info.hccis.admin.dao;

import info.hccis.admin.dao.util.ConnectionUtils;
import info.hccis.admin.dao.util.DbUtils;
import info.hccis.admin.model.DatabaseConnection;
import info.hccis.admin.model.entity.UserAccess;
import info.hccis.admin.model.jpa.CodeType;
import info.hccis.admin.util.Utility;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class UserAccessDAO {

    public static String getUserTypeCode(String username, String password) {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql;

        try {
            //connect to database
            String propFileName = "spring.data-access";
            ResourceBundle rb = ResourceBundle.getBundle(propFileName);
            String dbUserName = rb.getString("jdbc.username");
            String dbPassword = rb.getString("jdbc.password");
            //System.out.println("BJM-Set the database to "+database);

            //DatabaseConnection newConnection = new DatabaseConnection(database, dbUserName, dbPassword);
            conn = ConnectionUtils.getConnection();

            //create query to get usertypecode
            sql = "SELECT `userTypeCode` FROM `UserAccess` WHERE `username` = '" + username + "' AND `password`= '" + Utility.getHashPassword(password) + "'";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            //get data if statement returns a value
            String userTypeCode = null;
            while (rs.next()) {
                userTypeCode = Integer.toString(rs.getInt("userTypeCode"));
            }

            //return if data found
            if (userTypeCode != null) {
                return userTypeCode;
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("ERROR GETTING: User Type Code"
                    + "WHY: " + errorMessage);
        } finally {
            DbUtils.close(ps, conn);
        }
        //if data isn't found return 0
        return "0";
    }

    public static ArrayList<UserAccess> getUsers(DatabaseConnection databaseConnection) {
        ArrayList<UserAccess> users = new ArrayList();

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "SELECT * FROM `UserAccess` order by userAccessId";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                UserAccess user = new UserAccess();
                user.setUserAccessId(rs.getInt("userAccessId"));
                user.setUsername(rs.getString("username"));
                user.setUserType(rs.getInt("userTypeCode"));
                user.setUserTypeDescription(CodeValueDAO.getCodeValueDescription(databaseConnection, 1, user.getUserType()));
                //user.setCreatedDateTime(rs.getString("createdDateTime"));
                users.add(user);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }

        return users;
    }

    public static synchronized String insert(DatabaseConnection databaseConnection, UserAccess user) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        String results = "";

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "INSERT INTO `UserAccess`(`userAccessId`, `username`, `password`, `userTypeCode`, `createdDateTime`)"
                    + "VALUES (?,?,?,?,NOW())";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, user.getUserAccessId());
            ps.setString(2, user.getUsername());
            ps.setString(3, user.getPassword());
            ps.setInt(4, user.getUserType());
            ps.execute();
            results = "success";
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            results = "Fail";
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }
        return results;

    }

    public static synchronized String update(DatabaseConnection databaseConnection, UserAccess user) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "UPDATE `UserAccess` SET `username` = '" + user.getUsername() + "', `password` = '" + user.getPassword() + "', `userTypeCode` = " + user.getUserType() + " WHERE userAccessId = " + user.getUserAccessId();
            System.out.println(sql);
            ps = conn.prepareStatement(sql);
            ps.execute();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return "Test";
    }

    public static synchronized String delete(DatabaseConnection databaseConnection, int userId) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "DELETE FROM`UserAccess` WHERE userAccessId = " + userId;

            ps = conn.prepareStatement(sql);
            ps.execute();
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return "Test";
    }

    public static synchronized UserAccess selectUser(DatabaseConnection databaseConnection, int userId) throws Exception {
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        ResultSet rs = null;
        UserAccess userSelect = new UserAccess();
        System.out.println(userId);

        try {
            conn = ConnectionUtils.getConnection(databaseConnection);

            sql = "SELECT * FROM `UserAccess` WHERE userAccessId = " + userId;
            System.out.println(sql);
            ps = conn.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                userSelect.setUserAccessId(rs.getInt("userAccessId"));
                userSelect.setUsername(rs.getString("username"));
                userSelect.setPassword(rs.getString("password"));
                userSelect.setUserType(rs.getInt("userTypeCode"));
                //userSelect.setCreatedDateTime(rs.getString("createdDateTime"));
            }

            System.out.println(userSelect.getUsername());
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;

        } finally {
            DbUtils.close(ps, conn);
        }

        return userSelect;
    }
}
