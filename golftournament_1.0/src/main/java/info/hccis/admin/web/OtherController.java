package info.hccis.admin.web;

import info.hccis.admin.dao.UserAccessDAO;
import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.TournamentRepository;
import info.hccis.admin.model.entity.UserAccess;
import info.hccis.admin.model.jpa.Tournament;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;
    private final TournamentRepository tournamentRepository;

    @Autowired
    public OtherController(CodeTypeRepository ctr, CodeValueRepository cvr, TournamentRepository tournamentRepository) {
        this.ctr = ctr;
        this.cvr = cvr;
        this.tournamentRepository = tournamentRepository;
    }

    @RequestMapping("/other/futureUse")
    public String showFutureUse(Model model) {
        return "other/futureUse";
    }

    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") UserAccess user, HttpServletRequest request) {

        String FILE_NAME = "c:\\cis2232\\LoginAttempts.csv";
        /*
        
        See the @ModelAttribute refers to the object that is used on the form.  This is
        obtained here in the controller.  This is a common way that the controller gets access
        to the object and its attributes.  
        
         */
//Test to show what was passed in.  
        System.out.println("BJM in /authenticate, username passed in to controller is:" + user.getUsername());

        //check for access of the user
        String accessLevel = UserAccessDAO.getUserTypeCode(user.getUsername(), user.getPassword());

        
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {


            File file = new File(FILE_NAME);

            // if file doesnt exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // true = append file
            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);

            bw.write(user.getUsername() + ", " + user.getPassword() + ", " + accessLevel + ", ");


        } catch (IOException e) {

            e.printStackTrace();

        } finally {

            try {

                if (bw != null) {
                    bw.close();
                }

                if (fw != null) {
                    fw.close();
                }

            } catch (IOException ex) {

                ex.printStackTrace();

            }
        }
        
//        try {
//            try (PrintWriter writer = new PrintWriter(FILE_NAME, "UTF-8")) {
//                writer.println(user.getUsername() + ", " + user.getPassword() + ", " + accessLevel);
//            }
//        } catch (IOException e) {
//            System.out.println("Error updating file");
//        }


        System.out.println("Saved players to " + FILE_NAME);

        if (accessLevel.equals("0")) {
            return "/golf/welcome";
        } else {

            /*
        
        Here we will store the user in the session.  It will then be available to 
        other future requests.  
        
             */
            request.getSession().setAttribute("user", user);

            /*
        
        Set an object in the model to be used on the next form.  
        
             */
            /*
        
        Specify the return.  This is the next view that will be presented to the user.
             */
            ArrayList<Tournament> tournaments = (ArrayList<Tournament>) tournamentRepository.findAll();
            model.addAttribute("tournaments", tournaments);

            return "/golf/viewTournaments";
        }
    }

    @RequestMapping("/other/about")
    public String showAbout(Model model) {
     
        return "other/about";
    }

    @RequestMapping("/other/help")
    public String showHelp(Model model) {
        return "other/help";
    }


}
