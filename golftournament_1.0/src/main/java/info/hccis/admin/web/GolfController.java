package info.hccis.admin.web;

import info.hccis.admin.data.springdatajpa.CodeTypeRepository;
import info.hccis.admin.data.springdatajpa.CodeValueRepository;
import info.hccis.admin.data.springdatajpa.PlayerRepository;
import info.hccis.admin.data.springdatajpa.TeamRepository;
import info.hccis.admin.data.springdatajpa.TournamentRepository;
import info.hccis.admin.model.entity.UserAccess;
import info.hccis.admin.model.jpa.Tournament;
import info.hccis.admin.model.jpa.Team;
import info.hccis.admin.model.jpa.Player;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class GolfController {

    private final CodeTypeRepository ctr;
    private final CodeValueRepository cvr;
    private final TournamentRepository tournamentRepository;
    private final TeamRepository teamRepository;
    private final PlayerRepository playerRepository;

    @Autowired
    public GolfController(CodeTypeRepository ctr, CodeValueRepository cvr, TournamentRepository tour, TeamRepository tear, PlayerRepository pr) {
        this.ctr = ctr;
        this.cvr = cvr;
        this.tournamentRepository = tour;
        this.teamRepository = tear;
        this.playerRepository = pr;
    }

    @RequestMapping("/golf/addTournament")
    public String addTournament(Model model) {

        Tournament tournament = new Tournament();
        //tournament.setTournamentName("test");
        model.addAttribute("tournament", tournament);
        return "golf/addTournament";
    }

    @RequestMapping("/golf/addSubmitTournament")
    public String addSubmitTournament(Model model, @Valid @ModelAttribute("tournament") Tournament tournament, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation of tournament.");
            return "golf/addTournament";
        }
        try {
            tournamentRepository.save(tournament);
            ArrayList<Tournament> tournaments = (ArrayList<Tournament>) tournamentRepository.findAll();
            model.addAttribute("tournaments", tournaments);

            return "golf/viewTournaments";
        } catch (Exception ex) {
            System.out.println("There was an error adding the tournament.");
            return "";
        }
    }

    @RequestMapping("/golf/addTeam")
    public String addTeam(Model model) {

        Team team = new Team();
        model.addAttribute("team", team);
        return "golf/addTeam";
    }
    
    @RequestMapping("/golf/addSubmitTeam")
    public String addSubmitTeam(Model model, @Valid @ModelAttribute("team") Team team, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation of team.");
            return "golf/addTeam";
        }
        try {
            teamRepository.save(team);
            ArrayList<Team> teams = (ArrayList<Team>) teamRepository.findAll();
            model.addAttribute("teams", teams);

            return "golf/viewTeams";
        } catch (Exception ex) {
            System.out.println("There was an error adding the team.");
            return "";
        }
    }
    
    @RequestMapping("/golf/addSubmitPlayer")
    public String addSubmitPlayer(Model model, @Valid @ModelAttribute("player") Player player, BindingResult result) {

        if (result.hasErrors()) {
            System.out.println("Error in validation of player.");
            return "golf/addPlayer";
        }
        try {
            playerRepository.save(player);
            ArrayList<Player> players = (ArrayList<Player>) playerRepository.findAll();
            model.addAttribute("players", players);

            return "golf/viewPlayers";
        } catch (Exception ex) {
            System.out.println("There was an error adding the team.");
            return "";
        }
    }

    @RequestMapping("/golf/addPlayer")
    public String addPlayer(Model model) {
        Player player = new Player();
        model.addAttribute("player", player);
        return "golf/addPlayer";
    }

   @RequestMapping("/golf/editTeam")
    public String editTeam(Model model, HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("BJM-/squash/edit -->id=" + id);

        //get the tournament from the repository based on the id
        Team temp = teamRepository.findOne(id);
        //send the user back to the add pag after putting the squash object in the model
        model.addAttribute("team", temp);
        return "golf/addTeam";

    } 

   @RequestMapping("/golf/editPlayer")
    public String editPlayer(Model model, HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("BJM-/squash/edit -->id=" + id);

        //get the tournament from the repository based on the id
        Player temp = playerRepository.findOne(id);
        //send the user back to the add pag after putting the squash object in the model
        model.addAttribute("player", temp);
        return "golf/addPlayer";

    }

    @RequestMapping("/")
    public String showHome(Model model) {
        UserAccess user = new UserAccess();
        user.setUsername("cis2232_admin");
        user.setPassword("Test1234");
        model.addAttribute("user", user);
        return "golf/welcome";
    }

    @RequestMapping("/golf/viewTournaments")
    public String viewTournaments(Model model) {
        ArrayList<Tournament> tournaments = (ArrayList<Tournament>) tournamentRepository.findAll();
        model.addAttribute("tournaments", tournaments);
        return "golf/viewTournaments";
    }
    
    @RequestMapping("/golf/viewTeams")
    public String viewTeams(Model model) {
        ArrayList<Team> teams = (ArrayList<Team>) teamRepository.findAll();
        model.addAttribute("teams", teams);
        return "golf/viewTeams";
    }
    
    @RequestMapping("/golf/viewPlayers")
    public String viewPlayers(Model model) {
        ArrayList<Player> players = (ArrayList<Player>) playerRepository.findAll();
        model.addAttribute("players", players);
        return "golf/viewPlayers";
    }
    
    @RequestMapping("/golf/editTournament")
    public String editTournament(Model model, HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("BJM-/squash/edit -->id=" + id);

        //get the tournament from the repository based on the id
        Tournament temp = tournamentRepository.findOne(id);
        //send the user back to the add pag after putting the squash object in the model
        model.addAttribute("tournament", temp);
        return "golf/addTournament";

    }

}
