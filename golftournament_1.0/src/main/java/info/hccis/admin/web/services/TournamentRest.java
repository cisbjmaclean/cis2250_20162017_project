package info.hccis.admin.web.services;

import info.hccis.admin.model.jpa.Player;
import info.hccis.admin.model.jpa.Tournament;
import info.hccis.admin.model.jpa.Team;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import info.hccis.admin.data.springdatajpa.PlayerRepository;
import info.hccis.admin.data.springdatajpa.TournamentRepository;
import info.hccis.admin.data.springdatajpa.TeamRepository;
import java.text.SimpleDateFormat;
import java.util.Date;

@Path("tournaments")
public class TournamentRest {

    @Resource
    private final PlayerRepository playerRepository;
    private final TournamentRepository tournamentRepository;
    private final TeamRepository teamRepository;

    public TournamentRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.playerRepository = applicationContext.getBean(PlayerRepository.class);
        this.tournamentRepository = applicationContext.getBean(TournamentRepository.class);
        this.teamRepository = applicationContext.getBean(TeamRepository.class);
    }

    @GET
    @Path("/hello")
    public Response hello() {

        System.out.println("in controller for /player/hello");
        return Response.status(200).entity("hello").build();
    }

    @GET
    @Path("/upcoming/{startDate}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUpcomingTournaments(@PathParam("startDate") String startDate) {

        System.out.println("in controller for /player/list");
        ArrayList<Tournament> tournaments = (ArrayList<Tournament>) tournamentRepository.findAll();

        //************************************************
        // Use classes from the Jackson library to convert our
        // array list of objects to json.
        //*************************************************
        final ObjectMapper mapper = new ObjectMapper();
        boolean first = true;
        String temp = "[";

        try {
            for (Tournament tournament : tournaments) {
                if (startDate.compareTo(tournament.getTournamentStartDate()) <= 0) {
                if (temp.length()> 1) {
                        temp += ",";
                        first = false;
                    }
                    temp += mapper.writeValueAsString(tournament);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(TournamentRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        temp += "]";
        return Response.status(200).entity(temp).build();
    }

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTournaments() {

        System.out.println("in controller for /player/list");
        ArrayList<Tournament> tournaments = (ArrayList<Tournament>) tournamentRepository.findAll();
        boolean first = true;
        //************************************************
        // Use classes from the Jackson library to convert our
        // array list of objects to json.
        //*************************************************
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "[";
        try {
            for (Tournament tournament : tournaments) {
                System.out.println(temp + "  first=" + first);
                if (temp.length() > 1) {
                    temp += ",";
                    first = false;
                }

                temp += mapper.writeValueAsString(tournament);
            }
        } catch (IOException ex) {
            Logger.getLogger(TournamentRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        temp += "]";
        return Response.status(200).entity(temp).build();
    }

    /**
     * GET operation which will return a specific player based on the id passed
     * in.
     *
     * @param id
     * @return json representing the player.
     */
    @GET
    @Path("/players/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPlayersForTournament(@PathParam("id") int id) {

        System.out.println("in controller for /tournament/list");
        Tournament tournament = tournamentRepository.findOne(id);
        if (tournament == null) {
            return Response.status(204).entity("[]").build();
        }
        ArrayList<Team> teams = (ArrayList<Team>) teamRepository.findAll();
        ArrayList<Player> players = (ArrayList<Player>) playerRepository.findAll();

        final ObjectMapper mapper = new ObjectMapper();

        String temp = "[";
        boolean first = true;
        try {
            for (Team team : teams) {
                if (team.getTournamentId() == tournament.getTournamentId()) {
                    for (Player player : players) {
                        if (player.getTeamId() == team.getTeamId()) {
                            if (temp.length() > 1) {
                                temp += ",";
                                first = false;
                            }
                            temp += mapper.writeValueAsString(player);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(TournamentRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        temp += "]";
        return Response.status(200).entity(temp).build();
    }
}
