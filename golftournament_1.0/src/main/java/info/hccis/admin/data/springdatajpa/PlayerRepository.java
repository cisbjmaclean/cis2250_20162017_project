package info.hccis.admin.data.springdatajpa;


import info.hccis.admin.model.jpa.Player;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlayerRepository extends CrudRepository<Player, Integer> {


}