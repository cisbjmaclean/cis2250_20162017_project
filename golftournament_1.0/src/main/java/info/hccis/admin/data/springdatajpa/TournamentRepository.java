package info.hccis.admin.data.springdatajpa;


import info.hccis.admin.model.jpa.Tournament;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TournamentRepository extends CrudRepository<Tournament, Integer> {


}