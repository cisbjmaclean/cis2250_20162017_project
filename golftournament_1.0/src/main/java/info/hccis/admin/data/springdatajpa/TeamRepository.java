package info.hccis.admin.data.springdatajpa;


import info.hccis.admin.model.jpa.Team;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends CrudRepository<Team, Integer> {


}