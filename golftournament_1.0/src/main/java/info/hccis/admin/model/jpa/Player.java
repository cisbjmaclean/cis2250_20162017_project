/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.admin.model.jpa;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author sarsenault112452
 */
@Entity
@Table(name = "player")
@NamedQueries({
    @NamedQuery(name = "Player.findAll", query = "SELECT p FROM Player p")})
public class Player implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "playerId")
    private Integer playerId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "playerName")
    private String playerName;
    @Basic(optional = false)
    @NotNull
    @Column(name = "teamId")
    private int teamId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "playerShirtSize")
    private String playerShirtSize;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "playerEmail")
    private String playerEmail;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "playerFoodAllergies")
    private String playerFoodAllergies;

    public Player() {
    }

    public Player(Integer playerId) {
        this.playerId = playerId;
    }

    public Player(Integer playerId, String playerName, int teamId, String playerShirtSize, String playerEmail, String playerFoodAllergies) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.teamId = teamId;
        this.playerShirtSize = playerShirtSize;
        this.playerEmail = playerEmail;
        this.playerFoodAllergies = playerFoodAllergies;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public void setPlayerId(Integer playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getTeamId() {
        return teamId;
    }

    public void setTeamId(int teamId) {
        this.teamId = teamId;
    }

    public String getPlayerShirtSize() {
        return playerShirtSize;
    }

    public void setPlayerShirtSize(String playerShirtSize) {
        this.playerShirtSize = playerShirtSize;
    }

    public String getPlayerEmail() {
        return playerEmail;
    }

    public void setPlayerEmail(String playerEmail) {
        this.playerEmail = playerEmail;
    }

    public String getPlayerFoodAllergies() {
        return playerFoodAllergies;
    }

    public void setPlayerFoodAllergies(String playerFoodAllergies) {
        this.playerFoodAllergies = playerFoodAllergies;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (playerId != null ? playerId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Player)) {
            return false;
        }
        Player other = (Player) object;
        if ((this.playerId == null && other.playerId != null) || (this.playerId != null && !this.playerId.equals(other.playerId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Player[ playerId=" + playerId + ", playerName=" + playerName + ", teamId=" + teamId + ", playerShirtSize=" + playerShirtSize + ", playerEmail=" + playerEmail + ", playerFoodAllergies=" + playerFoodAllergies + " ]";
    }

}
