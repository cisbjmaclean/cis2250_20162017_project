create database cis2232_golftournament;
use cis2232_golftournament;

/*create a user in database*/
grant select, insert, update, delete on cis2232_golftournament.*
             to 'cis2232_admin'@'localhost'
             identified by 'Test1234';
flush privileges;

CREATE TABLE IF NOT EXISTS `CodeType` (
  `codeTypeId` int(3) NOT NULL COMMENT 'This is the primary key for code types',
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Dumping data for table `code_type`
--



CREATE TABLE IF NOT EXISTS `CodeValue` (
  `codeTypeId` int(3) NOT NULL COMMENT 'see code_type table',
  `codeValueSequence` int(3) NOT NULL,
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `englishDescriptionShort` varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `frenchDescriptionShort` varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `code_type`
--
ALTER TABLE `CodeType`
  ADD PRIMARY KEY (`codeTypeId`);

--
-- Indexes for table `code_value`
--
ALTER TABLE `CodeValue`
  ADD PRIMARY KEY (`codeValueSequence`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `code_type`
--
ALTER TABLE `CodeType`
  MODIFY `CodeTypeId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key for code types',AUTO_INCREMENT=1;

ALTER TABLE `CodeValue`
  MODIFY `codeValueSequence` int(3) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE CodeValue
    ADD FOREIGN KEY (codeTypeId)
    REFERENCES CodeType(codeTypeId);

--
-- Table structure for table `useraccess`
--

CREATE TABLE IF NOT EXISTS `UserAccess` (
  `userAccessId` int(3) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT 'Unique user name for app',
  `password` varchar(128) NOT NULL,
  `userTypeCode` int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  `createdDateTime` datetime DEFAULT NULL COMMENT 'When user was created.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE UserAccess
ADD FOREIGN KEY (userTypeCode)
REFERENCES CodeValue(codeValueSequence);

--
-- Indexes for table `useraccess`
--
ALTER TABLE UserAccess
  ADD PRIMARY KEY (`userAccessId`);

--
-- AUTO_INCREMENT for table `useraccess`
--
ALTER TABLE `UserAccess`
  MODIFY `userAccessId` int(3) NOT NULL AUTO_INCREMENT;


--
-- Dumping data for table `codevalue`
--

INSERT INTO `CodeType` (`CodeTypeId`, `englishDescription`, `frenchDescription`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 'User Types', 'User Types FR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

INSERT INTO `CodeValue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 1, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

delete from tournament;
delete from team;
delete from player;

drop table player;
drop table team;
drop table tournament;

CREATE TABLE `cis2232_golftournament`.`tournament` 
( `tournamentId` INT(6) NOT NULL AUTO_INCREMENT, 
`tournamentName` VARCHAR(80) NOT NULL , 
`tournamentAddress` VARCHAR(80) NOT NULL , 
`tournamentStartDate` VARCHAR(20) NOT NULL , 
`tournamentEndDate` VARCHAR(20) NOT NULL , 
PRIMARY KEY (`tournamentId`)) ENGINE = InnoDB;

CREATE TABLE `cis2232_golftournament`.`team` 
( `teamId` INT(6) NOT NULL AUTO_INCREMENT,
`tournamentId` INT(6) NOT NULL ,  
`teamName` VARCHAR(50) NOT NULL , 
`teamLeaderName` VARCHAR(50) NOT NULL , 
`teamContactNumber` VARCHAR(20) NOT NULL , 
`teamWebsite` VARCHAR(80) NOT NULL , 
PRIMARY KEY (`teamId`))
      ENGINE = InnoDB;

CREATE TABLE `cis2232_golftournament`.`player` 
( `playerId` INT(6) NOT NULL AUTO_INCREMENT, 
`playerName` VARCHAR(20) NOT NULL, 
`teamId` INT(6) NOT NULL,
`playerShirtSize` VARCHAR(10) NOT NULL , 
`playerEmail` VARCHAR(40) NOT NULL , 
`playerFoodAllergies` VARCHAR(50) NOT NULL , 
PRIMARY KEY (`playerId`))
      ENGINE = InnoDB;

